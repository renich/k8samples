k8samples
=========
Un repositorio de ejemplos fáciles de k8s. Ejemplos mínimos para ayudarte a entender como funciona y lo puedas usar.

Minikube
========
Si no tienes instalado minikube; he aquí una receta para Fedora 32:

.. code:: sh

    # Como root
    ## Instalar
    dnf -y install https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm

    ## Haz a tu usuario miembro de qemu y libvirt para
    ## prevenir facilitar el acceso (menos passwords)
    usermod -aG qemu,libvirt renich

    ## instalar kubectl
    ## https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-using-native-package-management
    cat <<- EOF > /etc/yum.repos.d/kubernetes.repo
    [kubernetes]
    name=Kubernetes
    baseurl=https://pkgs.k8s.io/core:/stable:/v1.30/rpm/
    enabled=1
    gpgcheck=1
    gpgkey=https://pkgs.k8s.io/core:/stable:/v1.30/rpm/repodata/repomd.xml.key
    EOF

    dnf -y install kubectl


    # Como usuario normal
    ## Hazte miembro de los grupos a los que te agregaste (o, logout y login)
    newgrp qemu
    newgrp libvirt

    ## Configurar
    ### La mayor cantidad de CPUs que puedas
    minikube config set cpus 8

    ### 8192 MiB de RAM sería un mínimo razonable
    minikube config set memory 16384

    ### de perdida unos 100 GiB de espacio
    minikube config set disk-size 200GiB

    ### Configúralo para usar el driver de kvm2... o podman
    minikube config set rootless true
    minikube config set driver podman

    ### Configurarlo para usar crio en vez de docker
    ### Recuerda cambiarlo a conainerd si no quieres usar sudo sin password para podman
    minikube config set bootstrapper kubeadm
    minikube config set container-runtime crio


    ## Iniciar
    minikube start

    ## Activar addons:
    ## No son esenciales; los puedes omitir o cambiar.
    ## Recuerda que el orden importa.
    addons=(
        metallb
        metrics-server
        dashboard
        registry
        volumesnapshots
        csi-hostpath-driver
        ingress
    )

    for addon in ${addons[@]}; do
        minikube addons enable $addon;
    done

    ## configurar metallb
    ### obtener la ip de tu instancia
    minikube ip

    ### configurar la IP para el balanceador de carga
    ### pon la misma IP dos veces
    minikube addons configure metallb

    ## Revisa todos los pods y espera a que inicien
    watch kubectl get -A pods

Una vez que tengas instalado minikube, podrás correr los ejemplos.

Para rrancar minikube, utiliza: ``minikube start``

Para detener minikube, utiliza: ``minikube stop``

Instrucciones
=============

Ejecutar un ejemplo
-------------------
Para poder correr uno de los ejemplos, solo usa el siguiente comando:

.. code:: sh

    kubectl create -f <algún-ejemplo.yaml>

Por ejemplo:

.. code:: sh

    kubectl create -f 01-nginx.yaml

Borrar los pods de un ejemplo
-----------------------------
Para borrarlo, solo debes usar:

.. code:: sh

    kubectl delete -f <algún-ejemplo.yaml>

Por ejemplo:

.. code:: sh

    kubectl delete -f 01-nginx.yaml

Monitorear pods
---------------
Es muy entretenido revelador monitorear k8s mientras corres los ejemplos. Te recomiendo usar el comando siguiente para hacerlo
contínuamente:

.. code:: sh

    watch kubectl get pv,pvc,svc,ingress,deployments,pods

Eso te mostrará todos los artefactos creados por el ejemplo; cuando éstos existan. Para salir del ``watch`` solo pulsa ``<ctrl> +
c``.

